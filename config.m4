dnl config.m4 for extension opengl

PHP_ARG_WITH(freeglut, for freeglut support,
[  --with-freeglut[=DIR]       Include FreeGLUT support])

dnl CFLAGS="$CFLAGS -Wall -Wfatal-errors -shared -framework GLUT"
CFLAGS="$CFLAGS -Wall -Wfatal-errors -shared -framework GLUT"

if test "$PHP_FREEGLUT" != "no"; then

  $INCLUDE_DIR=""

  if test -r $PHP_FREEGLUT/include/GL/freeglut.h; then
    INCLUDE_DIR=$PHP_FREEGLUT
  else
    AC_MSG_CHECKING(for OpenGL development files in default path)
    for i in /usr/X11R6 /usr/local /usr; do

      if test -r $i/include/GL/freeglut.h ; then
        INCLUDE_DIR=$i
        AC_MSG_RESULT(found in $i)
      fi

      if test -r $i/gl.h ; then
        INCLUDE_DIR=$i
        AC_MSG_RESULT(found in $i)
      fi

    done
  fi

  if test -z "$INCLUDE_DIR"; then
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR(Please make sure FreeGLUT is properly installed)
  fi

  AC_CHECK_PROG(SWIG_CHECK,swig,yes)
  if test x"$SWIG_CHECK" != x"yes" ; then
    AC_MSG_ERROR([Please install swig before installing.])
  fi

  AC_CONFIG_COMMANDS([swig], swig -php7 -I"$INCLUDE_DIR" -I"$INCLUDE_DIR/include" freeglut.i, [INCLUDE_DIR=$INCLUDE_DIR])

  PHP_ADD_INCLUDE($INCLUDE_DIR/include)
  PHP_ADD_INCLUDE($INCLUDE_DIR)

  PHP_SUBST(FREEGLUT_SHARED_LIBADD)
  PHP_NEW_EXTENSION(freeglut, freeglut_wrap.c, $ext_shared)
fi
