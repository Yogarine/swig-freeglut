%module freeglut
%{
#include <GL/freeglut.h>
#include <GL/freeglut_std.h>
#include <GL/freeglut_ext.h>
%}
%ignore glutStrokeRoman;
%ignore glutStrokeMonoRoman;
%ignore glutBitmap9By15;
%ignore glutBitmap8By13;
%ignore glutBitmapTimesRoman10;
%ignore glutBitmapTimesRoman24;
%ignore glutBitmapHelvetica10;
%ignore glutBitmapHelvetica12;
%ignore glutBitmapHelvetica18;

%include <cpointer.i>
%include <carrays.i>

%pointer_functions(int, intp)
%array_functions(char *, string_array);


%include "GL/freeglut.h"
%include "GL/freeglut_std.h"
%include "GL/freeglut_ext.h"
